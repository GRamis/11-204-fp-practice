data Tree = Empty | Node Tree Integer Tree deriving (Show, Read, Eq)

tree1 = Empty
tree2 = Node Empty 10 Empty
showTree tree = show tree

singleton :: Integer -> Tree
singleton x = Node  Empty x Empty

treeInsert :: Integer -> Tree  -> Tree 
treeInsert x Empty = singleton x

treeInsert x (Node left a right)
     | x == a = Node left x right
     | x < a  = Node (treeInsert x left) a  right
     | x > a  = Node left a (treeInsert x right)
	 
--Высота дерева	 
heightTree :: Tree -> Integer
heightTree Empty = 0
heightTree (Node l _ r) = (max (heightTree l) (heightTree r)) + 1

--Поиск
treeSearch :: Tree -> Integer -> Tree
treeSearch Empty _ = (Empty)
treeSearch (Node left x right) k 
	| k > x = Node left x (treeSearch right k)
	| k < x = Node (treeSearch left k) x right
	
	